import java.text.DecimalFormat;
import java.util.*;

public class ProgramVoucher {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Total Point yang dimiliki = ");
        int totalPointYangDimiliki = input.nextInt();
//        int totalPointYangDimiliki = 1150;
        LinkedHashMap<Integer,Integer> voucherMap = new LinkedHashMap<>();
        voucherMap.put(100000, 800);
        voucherMap.put(50000, 400);
        voucherMap.put(25000, 200);
        voucherMap.put(10000, 100);

        System.out.println("Total Point yang dimiliki = " + totalPointYangDimiliki + " point");
        for(Map.Entry<Integer,Integer> entry : voucherMap.entrySet()){
            int vocValue = entry.getKey();
            int vocPoint = entry.getValue();

            int count = totalPointYangDimiliki / vocPoint;
            totalPointYangDimiliki %= vocPoint;
            DecimalFormat decimalFormat = new DecimalFormat("#,###.##");

            if (count>0){
                for (int i = 0; i < count; i++){
                    System.out.println("Voucher yang didapat = " + "Rp. "+ decimalFormat.format(vocValue) +" -> " + vocPoint + " point");
                }
            }
        }

        if(totalPointYangDimiliki > 0){
            System.out.println("Sisa Point = " + totalPointYangDimiliki + " point");
        }else {
            System.out.println("Sisa Point = 0");
        }
    }
}
